import argparse
from archiver.mod import run as run_archiver
from parse import validate_urls

parser = argparse.ArgumentParser(
                    prog='warchpy',
                    description='pushing to webarchive sites by urls')

parser.add_argument("mode")
# parser.add_argument("--input-file", action="store", nargs=1)
# parser.add_argument("--output-file", action="store", nargs=1)
parser.add_argument("urls", action="store", nargs="*")
parser.add_argument("--json", action="store_true")

args = parser.parse_args()

print(args)

match args.mode:
    case "archiver" | "a":
        urls = validate_urls(args.urls)
        run_archiver(urls)
    case "scrapper" | "s":
        print("ok")
    case other:
        print("dont know this mode, try archiver or scrapper")
