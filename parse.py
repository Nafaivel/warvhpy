import validators

def validate_urls(urls: list[str]) -> list[str]:
    for url in urls:
        if validators.url(url):
            print(url)
        else:
            print("ERR: ",url)
            raise(BrokenPipeError)
    return urls
