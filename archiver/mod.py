from time import sleep
from waybackpy import WaybackMachineSaveAPI
from waybackpy.exceptions import TooManyRequestsError, MaximumSaveRetriesExceeded
from colorama import Fore, Style
import sys


def run(urls: list[str] = []):
    waited_before = False
    ok_amount = 0
    print("start archiving")
    for url in urls:
        # print(f"archiving url: {url}")
        wb = WaybackMachineSaveAPI(
            url,
            user_agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36",
        )
        try:
            print(
                Fore.GREEN + "OK" + Style.RESET_ALL + f"  {wb.save()} {wb.timestamp()}"
            )
            ok_amount += 1
        # except TooManyRequestsError:
        #     if not waited_before or ok_amount >= 15:
        #         handle_wait_err(wb, url)
        #     else:
        #         print(Fore.RED + f"RQSTS" + Style.RESET_ALL + f" {url}")
        #     waited_before = True
        except KeyboardInterrupt:
            sys.exit(0)
        except MaximumSaveRetriesExceeded:
            print(Fore.RED + f"RETR" + Style.RESET_ALL + f" {url}")
        except:
            print(Fore.RED + f"FAIL" + Style.RESET_ALL + f" {url}")


def handle_wait_err(wb: WaybackMachineSaveAPI, url: str):
    try:
        print("Too many request wait 5 minutes")
        for i in range(299, 0, -1):
            print(i, end="\r")
            sleep(1)
        print(Fore.GREEN + "OK" + Style.RESET_ALL + f"  {wb.save()} {wb.timestamp()}")
    except KeyboardInterrupt:
        sys.exit(0)
    except:
        print(Fore.RED + f"RQSTS" + Style.RESET_ALL + f" {url}")
